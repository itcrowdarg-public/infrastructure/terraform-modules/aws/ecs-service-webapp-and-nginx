# Gets the CURRENT task definition from AWS, reflecting anything that's been deployed
# outside of Terraform (ie. CI builds).
# source: https://github.com/hashicorp/terraform-provider-aws/issues/632#issuecomment-341127191
data "aws_ecs_task_definition" "web_service" {
  task_definition = "${aws_ecs_task_definition.web_service.family}"
}

resource "aws_ecs_service" "web_service" {
  name            = "web-${var.ecs_service_name}-${var.environment}"
  cluster         = var.environment
  task_definition = "${aws_ecs_task_definition.web_service.family}:${max("${aws_ecs_task_definition.web_service.revision}", "${data.aws_ecs_task_definition.web_service.revision}")}"
  desired_count   = var.web_service_configuration.service.desired_count
  iam_role        = aws_iam_role.cluster_service_role.arn
  depends_on      = [
    aws_iam_policy.cluster_service_policy,
    aws_iam_policy_attachment.cluster_service_policy_attachment,
    aws_iam_role.cluster_service_role
  ]

  deployment_maximum_percent         = var.web_service_configuration.service.deployment_maximum_percent
  deployment_minimum_healthy_percent = var.web_service_configuration.service.deployment_minimum_healthy_percent
  ordered_placement_strategy {
    type  = "spread"
    field = "host"
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.web.arn
    container_name   = "nginx"
    container_port   = 80
  }

}
