# ECS Service - WebApp and Nginx

## Usage

```terraform

module "ecs_service_rails_nginx" {
  source   = "git::https://gitlab.com/itcrowdarg-public/infrastructure/terraform-modules/aws/ecs-service-webapp-and-nginx.git?ref=2.0.1"

  environment     = each.value.name
  ecs_service_name = "backend" # [backend, frontend]

  web_service_configuration = var.web_service_configuration
  web_service_env_variables = concat(
    var.web_service_configuration.webapp.env_vars,
    var.task_def_common_env_vars, [
      { name = "REDIS_URL", value = "redis://${module.ecs_service_redis[each.key].service_url}" },
      { name = "DATABASE_HOST", value = module.rds_cluster[each.key].cluster_endpoint },
      { name = "DATABASE_NAME", value = module.rds_cluster[each.key].cluster_database_name },
      { name = "DATABASE_USER", value = module.rds_cluster[each.key].cluster_master_username },
      { name = "DATABASE_PASS", value = module.rds_cluster[each.key].cluster_master_password },
      { name = "DATABASE_READER_HOST", value = module.rds_cluster[each.key].cluster_reader_endpoint },
    ]
  )

  vpc                                   = module.vpc
  region                                = var.region
  listener_https                        = module.load_balancer.listener_https

  alb_healthcheck_path = "/healthcheck"
}
```

On /environments/staging.tfvars:

```terraform
environment = "staging"

task_def_common_env_vars = [
  { name = "RAILS_ENV", value = "staging" },
  { name = "RAILS_LOG_TO_STDOUT", value = "1" },
]

web_service_configuration = {
  service = {
    desired_count                      = 1
    deployment_maximum_percent         = 100
    deployment_minimum_healthy_percent = 0
    alb_rule_path                      = "/*"                  # "/*" all paths
    alb_rule_domain                    = "*.elb.amazonaws.com" # "*.elb.amazonaws.com" this LB
  }
  webapp = {
    image  = "itcrowdarg/rails-multibase:mysql"
    cpu    = 256
    memory = 256
    env_vars =  [
      { name = "RAILS_ENV", value = "staging" },
      { name = "MASTER_KEY", value = "THE_MASTER_KEY" },
      { name = "RAILS_LOG_TO_STDOUT", value = "1" },
    ]
    mount_opt = true # mount READ ONLY [host]/opt/docker_opt_volume to [container]/opt/docker_opt_volume
  }
  nginx = {
    image  = "itcrowdarg/nginx-docker-proxy:latest"
    cpu    = 256
    memory = 100
    env_vars = [
      { name = "DOCKER_NAME", value = "webapp" },
      { name = "DOCKER_PORT", value = "3000" },
      { name = "REQUEST_TIMEOUT", value = "60" },
      { name = "LISTEN_PORT", value = "80" },
      { name = "ROOT_PATH", value = "/app/public" },
    ]
  }

}
```
