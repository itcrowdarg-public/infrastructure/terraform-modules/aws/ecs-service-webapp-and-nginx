variable "vpc" {
  description = "VPC where the ASG is created"
}

variable "region" {
  description = "AWS region"
  type        = string
}

variable "listener_https" {
  description = "HTTPS listener"
}

variable "environment" {
  description = "The name of the environment"
  type        = string
}

variable "ecs_service_name" {
  description = "The name of the ECS service"
  type        = string
}

variable "alb_healthcheck_path" {
  description = "The path for the app healthcheck"
  type        = string
}

variable "web_service_configuration" {
  description = "Web service variables"
  type        = any
}

variable "web_service_env_variables" {
  description = "Environment variables for the task definition"
  type        = any
}
