locals {
  # Get the container name from the webapp, from the env vars of the nginx container   
  webapp_container_name = var.web_service_configuration.nginx.env_vars[index(var.web_service_configuration.nginx.env_vars.*.name, "DOCKER_NAME")].value
}

resource "aws_ecs_task_definition" "web_service" {
  family = "web-${var.ecs_service_name}-${var.environment}"

  volume {
    name      = "opt_volume"
    host_path = "/opt/docker_opt_volume"
  }

  container_definitions = jsonencode([
    {
      name              = local.webapp_container_name
      image             = var.web_service_configuration.webapp.image
      cpu               = var.web_service_configuration.webapp.cpu
      memoryReservation = var.web_service_configuration.webapp.memory
      essential         = true
      environment       = var.web_service_env_variables
      mountPoints       = var.web_service_configuration.webapp.mount_opt ? [
        {
          "sourceVolume" : "opt_volume",
          "containerPath" : "/opt/docker_opt_volume",
          "readOnly" : true
        }
      ] : []
      logConfiguration = {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : aws_cloudwatch_log_group.web.name,
          "awslogs-region" : var.region,
          "awslogs-stream-prefix" : "ecs",
        },
      },
    },
    {
      name              = "nginx"
      image             = var.web_service_configuration.nginx.image
      cpu               = var.web_service_configuration.nginx.cpu
      memoryReservation = var.web_service_configuration.nginx.memory
      essential         = true
      environment       = var.web_service_configuration.nginx.env_vars
      portMappings = [
        {
          containerPort = 80
          hostPort      = 0
        }
      ],
      links = [
        local.webapp_container_name
      ],
      volumesFrom = [
        {
          sourceContainer = local.webapp_container_name
          readOnly        = true
        }
      ],
      logConfiguration = {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : aws_cloudwatch_log_group.web.name,
          "awslogs-region" : var.region,
          "awslogs-stream-prefix" : "ecs",
        },
      },
    }
  ])
}
