resource "aws_alb_target_group" "web" {
  name = "tg-web-${var.ecs_service_name}-${var.environment}"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    timeout             = "3"
    path                = var.alb_healthcheck_path
    unhealthy_threshold = "2"
  }

  port     = "80"
  protocol = "HTTP"
  vpc_id   = var.vpc.vpc_id

}

resource "aws_lb_listener_rule" "web" {
  listener_arn = var.listener_https.arn

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.web.arn
  }

  condition {
    path_pattern {
      values = [var.web_service_configuration.service.alb_rule_path]
    }
  }

  condition {
    host_header {
      values = [var.web_service_configuration.service.alb_rule_domain]
    }
  }
}

