resource "aws_cloudwatch_log_group" "web" {

  name              = "web-${var.ecs_service_name}-${var.environment}"
  retention_in_days = 30

}
